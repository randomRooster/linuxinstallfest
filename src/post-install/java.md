# Java Development Kit (JDK)
Java is a is a general-purpose programming language that is taught in first year Computer Science at UoB. The development kit is the package you need in order to compile and run Java applications.

**Notice:** Java and JavaScript are two very different languages - it has as much in common as a Car and a Carpet.

## Installation
Manjaro:

    yes | sudo pacman -Syu jre-openjdk

Ubuntu:

    sudo apt install -y default-jdk

## Running
In order to verify that Java has been installed correctly, run the following command.

    java --version

This should produce the following (or similar) output if working correctly.
![../images/java-version.png](../images/java-version.png)

## Removal
Manjaro:

    yes | sudo pacman -Rs jre-openjdk

Ubuntu:

    sudo apt remove -y default-jdk
