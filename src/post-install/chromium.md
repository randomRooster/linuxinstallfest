# Chromium
Chromium is an open-source web browser.

## Installation
Manjaro:

    yes | sudo pacman -Syu chromium

Ubuntu:

    sudo apt install -y chromium-browser

## Running
You can either run Chromium by searching it in the applications menu (by pressing the Super (Windows) key and typing `Chromium`), or by running the following command:

    chromium

## Removal
Manjaro:

    yes | sudo pacman -Rs chromium

Ubuntu:

    sudo apt remove -y chromium-browser
