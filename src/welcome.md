# Welcome to Linux InstallFest!

This is a step-by-step guide, with detailed explanations on how to install
Linux.

This isn't meant to be an entirely standalone guide, however, it should be a
pretty good collection of our understanding of how to install Linux. If
you're around on Wednesday 30th of September, then make sure to join us on
[our discord](https://discord.com/invite/EkcRZc4), so we can help you out!
