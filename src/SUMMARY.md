# Summary

- [Welcome](./welcome.md)

---
- [README before anything else!](./disclaimer.md)
- [Install methods](./methods.md)
- [Dualboot preinstall checks](./dualboot-preinstall.md)
- [Virtual Machine Pre-install](./vm-preinstall.md)
- [Install](./install.md)
    - [Ubuntu](./install/ubuntu.md)
      - [Just Ubuntu](./install/ubuntu/pure.md)
      - [Dualboot](./install/ubuntu/dualboot.md)
      - [VM](./install/ubuntu/vm.md)
      - [WSL](./install/ubuntu/wsl.md)
    - [Manjaro](./install/manjaro.md)
      - [Just Manjaro](./install/manjaro/pure.md)
      - [Dualboot](./install/manjaro/dualboot.md)
      - [VM](./install/manjaro/vm.md)
- [Post-install](./post-install.md)
  - [Chromium](./post-install/chromium.md)
  - [Java](./post-install/java.md)
  - [Neofetch](./post-install/neofetch.md)
  - [Speedtest](./post-install/speedtest.md)
- [Post-install for VMs](./vm-post-install-ga.md)
- [Troubleshooting](./troubleshooting.md)

---

- [Learning](./learning.md)
