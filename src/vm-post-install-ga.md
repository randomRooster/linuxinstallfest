# Virtual Machine Post Installation

This guide will tell you how to enable a few usefull features for your VM,
after you have installed it. Before you follow this guide, make sure to 
update your system, as described in the [Post
Installation](./post-install.md) guide.

## Installing Guest Additions

Guest Additions from VirtualBox will allow you to enable some very useful 
features, such as bi-directional shared clipboard (i.e. you'll be able to
copy-paste between you host and the VM) and shared folders. Let's get started!

_Note: this guide was written using the Manjaro VM, but it is the same for 
the Ubuntu VM. Where the commands differ, both are provided._

With the VM powered off, go to Settings -> Storage. Select the CD with a plus
button next to Controller IDE.

_Note: if you still see the Linux ISO under the Controller IDE, **remove
it**! If you fail to remove the ISO, the VM will boot from the Live CD, not
from the VM disk!_

![./images/insert-ga-disk_1.png](./images/insert-ga-disk_1.png)

From the list, select `VBoxGuestAdditions.iso`. Press Choose, then OK. Start the VM.

![./images/insert-ga-disk_2.png](./images/insert-ga-disk_2.png)

Open up a terminal and install **gcc, make** and **perl**:

For Manjaro:

    sudo pacman -S gcc make perl

For Ubuntu:

    sudo apt install -y gcc make perl

If you are running in a Manjaro VM, you need to install the linux headers for
your kernel. You can confirm the kernel version by running

    uname -a

Example output:

    Linux neko3-manjaro 5.8.6-1-MANJARO #1 SMP PREEMPT Thu Sep 3 14:19:36 UTC 2020 x86_64 GNU/Linux

So 5.8.6-1 is my kernel version. 

Ubuntu installs these automatically, so no need to worry.

For Manjaro (58 matches the first two digits of the `uname -a` command):

    sudo pacman -S linux58-headers

Go to the Guest Additions folder. Replace `[user]` with your own
as needed. (Note that the GA version might have changed meanwhile)

For Manjaro:

    cd /run/media/[user]/VBox_GAs_6.1.14

For Ubuntu: 

    cd /media/[user]/VBox_GAs_6.1.14


![./images/cd-ga.png](./images/cd-ga.png)

Run the Linux GA:
    sudo ./VBoxLinuxAdditions.run

Provide **your user** password and confirm you want to install by typing `yes`.
It will then finish.

![./images/ga-done.png](./images/ga-done.png)

Next, let's make sure to add our used to the `vboxsf` group, such that we can do
shared folders (replace `neko` with your own username).

    sudo usermod -a -G vboxsf neko

Reboot! (You can optionally now remove the Guest Additions ISO from Settings -> Storage)

## Bi-directional clipboard

It's very useful to be able to copy between the guest VM and the host. I
usually use the browser on my host machine, and often times I find myself
copying stuff from the host to the VM. In order to enable the clipboard, just
do to Devices -> Shared Clipboard and choose which one suits you best. I
always go for Bidirectional :-) All done, it should just work now!

## Shared folders

It's also super useful to have a shared folder between your VM and host. This
means you don't have to copy across text or files, they can be seen by both
the VM and the host!

In order to setup a shared folder, we'll start with the VM powered off. Now
go to Settings -> Shared Folders. Click the folder with plus icon on the
right hand side. For the Folder Path box, click the dropdown button ->
Other... and navigate to the folder you want to share! In my case, I'll share
a folder called `docs` in my Documents. You can leave the Folder Name as it 
defaults to, or you can give it a different name. This is how the folder
will be named in the VM! You can also make it auto-mount, and give it the
path **in the VM** it will appear under. Remember, in Linux, your home
directory is always `/home/[your username]`. In my case, I chose to mount
under the `docs` directory my home directory. Click Ok until you exit
Settings. Now start your VM!

![./images/ga-shared-folder.png](./images/ga-shared-folder.png)

Now you should see your shared folder in your home directory! Congrats!

![./images/ls-shared-folder.png](./images/ls-shared-folder.png)


