# Post installation

Things you might want to do after installing linux, common software choices,
etc.

## Updating

You should keep your system up-to-date, and try and run updates every so
often. Updates aren't as naggy as in Windows, so you may find yourself
needing to remember to install them.

**Notice:** Make sure to be connected to an active internet connection, and
*prepare for the command to take a while to finish executing.

In Ubuntu (with apt):

    $ sudo apt update && sudo apt upgrade -y

In Manjaro (with pacman):

    $ sudo pacman -Syu

## Installing software

In Ubuntu (with apt):

    $ sudo apt install <package>

**Note:** you may see both `apt` and `apt-get` around. `apt` is the *new* form
*of `apt-get` and should be preferred in general (though not for scripts).

In Manjaro (with pacman):

    $ sudo pacman -S <package>

## Change your default shell

Lots of us tend to prefer zsh over bash. It's faster, more customizable, and
usually has fewer pain points.

To install it:

    $ sudo apt install zsh

or

    $ sudo pacman -S zsh

Then:

    $ chsh -s /bin/zsh

## Theming

One of the best things about using Linux is the awesome levels of
customisability!

How you customize the look and feel depends on what desktop environment
you've chosen, for Ubuntu, it's most likely GNOME, for Manjaro, it's most
likely KDE, but check to make sure.

Check out the settings menus in both!

### GNOME

For GNOME, you can install GNOME Tweak Tool to be able to change themes:

    $ sudo apt install gnome-tweak-tool

or

    $ sudo pacman -S gnome-tweaks

## Useful packages

+ [Chromium Web Browser](./post-install/chromium.md) - Chromium is an open-source web browser.
+ [Java Development Kit (JDK)](./post-install/java.md) - Java is a is a general-purpose programming language that is taught in first year Computer Science at UoB. 
+ [Neofetch](./post-install/neofetch.md) - Neofetch is a command that lists information about your Linux distribution and the machine you are running on.
+ [Speedtest](./post-install/speedtest.md) - Speedtest is a command that tests how fast your internet is via the terminal.
 
