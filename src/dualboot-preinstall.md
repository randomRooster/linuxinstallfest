# Before proceeding with installation

<span style="color:red; font-weight:bold"> IF YOU HAVE BITLOCKER ENABLED ON
YOUR WINDOWS, PLEASE DISABLE IT BEFORE PROCEEDING ANY FURTHER. If you fail to
disable it, your Windows may <em>fail to boot</em> after disabling Secure
Boot. If this happens, your only chance is if you have a recovery key in your
Microsoft account. Just disable it while you are doing the installation, you
can enable it afterwards!</span>

Also, please don't forget to make a **backup** of any important data!

## What is the BIOS and why do we care?

The BIOS is the part of your computer responsible for managing hardware and booting other operating systems.

![BIOS diagram](images/bios_diagram.png)

The BIOS is where we configure some of the lowest level hardware to make sure it's compatible before we mess around with our operating systems, so we're going to need to interact with it when preparing to dualboot.

Since the vast majority of laptops come with Windows preinstalled, their BOIS often come preconfigured with settings which may cause issues with Linux. Changing these settings should have minimal impact on your Windows installation, but as with all low-level tweaks, verify your backups before changing anything.

Here are a few common things you should configure in the BIOS before going ahead:


## Boot order

Boot order is very important to actually be able to boot to the installation media, like a USB!

By default, the system will try and boot off the first hard drive/solid state
drive available, however, modern computers can boot off of many different
media types, including network, cd drives, usb drives, etc.

In your BIOS, locate a section for "Boot Order", and rearrange them to place
USB at the very top, before any other method. It's possible that your BIOS
doesn't have this setting - which is very sad. If it doesn't, then when you
try to boot your installation media, you need to interrupt the boot process
and manually select the USB drive.

## Secure boot

[//]: <> (Technically only UEFI supports Secure Boot. Or would that be too pedantic for the average Joe and Jane?)

We're going to need to disable this to allow the Linux installer (and eventually OS itself) to boot, but don't worry: the chance hackers will be able to exploit that is very small! Once you are done, you can sign the Linux kernel and re-enable it.

[//]: <> (Maybe make a section for kernel signing?)

Basically, the idea of secure boot is that when booting into an OS the BIOS checks against a *cryptographic signature*. The issue here is that by default, your computer only trusts Microsoft, so when you try to boot into an OS that isn't Windows, the check against the cryptographic signature will fail and it will refuse to boot. This includes the Linux installer.
It will also impact your ability to run virtual machines, which will be an essential part of your digital toolkit.

[//]: <> (Can someone cite/verify the virtual machine part?)

You can find more information on Secure Boot [here](https://www.minitool.com/lib/secure-boot.html).

You'll generally find this under the BOOT tab. Switch it to **Disabled**.

## Fast Startup

Fast Startup is a feature of Windows that enables faster by saving the state of the Windows kernel and drivers to the disk, not too dissimlar from hibernation. This means that it doesn't have to spend time initialising parts of the OS that never change. However, just like with hibernation, this locks the *C:* partition to prevent changes while the OS is 'shut down'. Disabling Fast Boot
will make your Windows boot times *slightly* slower, but probably only by a few seconds at worst.

![https://www.howtogeek.com/wp-content/uploads/2018/04/img_5ad13471ebf4f.png](https://www.howtogeek.com/wp-content/uploads/2018/04/img_5ad13471ebf4f.png)



## Intel RST

Intel Rapid Storage Technology can generally speed up your system and make it
feel more responsive, however, because of a lack of drivers on Linux, it's
quite common that your installer *will not detect* your hard drive if you
have it enabled.

You can disable it, usually, by modifying your hard drive to use plain AHCI
instead of Intel Optane in the "Devices" section of the BIOS. However, before
you do this, be aware that you may take a small performance impact and notice
your hard drive speeds a bit slower than before. Also, note that if you're
planning on keeping Windows around, you'll need to do some prep before
making the change.

To avoid the chance of something going wrong, follow the guide on the Ubuntu
discourse [here](https://discourse.ubuntu.com/t/ubuntu-installation-on-computers-with-intel-r-rst-enabled/15347),
taking special note of the required instructions on the Windows side of
things.
