# README before anything else!

If you are installing it as a replacement OS or **dualboot**, please make sure
you have a **backup** of any data you do not want to lose. We are providing the
instructions as they are, and **we are not responsible for any data loss that
might occur**. By using them you agree **you are assuming your own risk**.

If you are installing it as a replacement OS or dualboot, you will need:
- a **USB stick**, of at least **4 GB**
- image of the OS you want to install, either:
  - Ubuntu: [Ubuntu](https://ubuntu.com/download/desktop)
  **or**
  - Manjaro: [Manjaro](https://manjaro.org/downloads/official/gnome/)
  
Other distributions are available, but they are outside the scope of this book.
