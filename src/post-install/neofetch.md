# Neofetch
Neofetch is a command that lists information about your Linux distribution and the machine you are running on, it also provides an ASCII art that differs based on the flavour of linux you are running. An alternative command to **neofetch** is **screenfetch**, which you can install and run in exactly the same way, just removing each instance of neofetch with screenfetch in the following commands.

## Installation
Manjaro:

    yes | sudo pacman -Syu neofetch

Ubuntu:

    sudo apt install -y neofetch

## Running
In order to run Neofetch simply execute the command via terminal.

    neofetch

Here are some example outputs, the messages will differ based on the version of Linux installed and your system's specifications.
![../images/neofetch-arch.png](../images/neofetch-arch.png)
![../images/neofetch-ubuntu.png](../images/neofetch-ubuntu.png)
## Uninstallation
Manjaro:

    yes | pacman -Rs neofetch

Ubuntu:

    sudo apt remove -y neofetch


