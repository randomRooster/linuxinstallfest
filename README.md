# Linux InstallFest Guide

Written guide to install Linux. Helper material for Linux InstallFest.

Uses [mdbook](https://github.com/rust-lang/mdBook) to generate a gitbook.

## Development

To run a live preview of the site, download and install `mdbook` (`cargo install mdbook`), then:

    $ mdbook serve --port 8000

Alternatively, use the provided docker-compose setup (exactly the same
versions and setup as production):

    $ docker-compose up

Whichever method you choose, you should have a live view of the site running
on <http://localhost:8000>.

## Contributing

Please feel free to contribute to the book. We are looking for improvements to
the existing sections for now. We want the instructions to be accompanied by 
screenshots for all the steps.

Thank you!
